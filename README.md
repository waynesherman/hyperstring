                          *** HyperString v6.0 ***
                          (c)1996-2003 EFD Systems
                             efd@mindspring.com

     See below for license agreement, installation, and use.

    Introduction ---------------------------------------------------------

    Welcome to HyperString!

    One of the most significant new features with Delphi32 is long dynamic
    strings.  However, the built-in functions don't really exploit the full
    potential of these new strings. HyperString provides over 400 fast,
    efficient string management routines to help you realize the full power
    of this highly versatile new data type.


    Installation ---------------------------------------------------------

    NOTE: As of v5, all form, shell and printer related code has been moved
    to a separate unit (HyperFrm). The handful of affected routines have
    been clearly noted in the docs. This was done to reduce VCL overhead in
    non-visual (console mode) apps.

    HyperString is a NON-VISUAL code library not unlike "Windows", "Forms",
    "SysUtils" or any of the other system code units commonly listed in the
    "uses" clause. Installation is simple but very different from that of a
    component; there is nothing to register within the Delphi environment.

    1) Copy HYPERSTR.PAS and HYPERFRM.PAS (or the .DCU) to your Delphi
       Library directory.
       (..\DELPHI\LIB)

    2) Copy HYPERSTR.HLP and HYPERSTR.CNT to your Delphi Help directory.
       (..\DELPHI\HELP)

    3) (Optional, but highly recommended) With minimal effort, the WinHelp
       HyperString docs can be seamlessly integrated into Delphi's own
       on-line Help for convenient access during coding.

       DELPHI 5+ ----------------------------------------------------------

       Select "Help|Customize|Edit|Add Files" and add HYPERSTR.CNT to
       the file list. Select "File|Save" and you're done!


       DELPHI 3/4 ---------------------------------------------------------

       First, make a backup copy of ..\DELPHI\HELP\DELPHI?.CNT

       Edit DELPHI?.CNT (it's a simple text file) using NOTEPAD, WORDPAD or
       the ASCII editor of your choice.  Insert 2 lines as follows:

       --------------------------------------------------------------
       1) Add to Index Section   > :Index HyperString Library =hyperstr.hlp
          (near top of file)

       2) Add to Include Section > :Include hyperstr.cnt
          (near end of file)
       --------------------------------------------------------------

       The colons are a part of each line. The modified DELPHI.CNT should
       look something like this:

       --------------------------------------------------------------
          :Base delphi.HLP>main
          :Title Delphi Help
          :Index HyperString Library =hyperstr.hlp
                    .
                    .
                    .
          :Include hyperstr.cnt

       --------------------------------------------------------------

       NOTE: Be sure to press [Enter] after typing the last line in the
       file in order to insert a hard CRLF. WinHelp will sometimes ignore a
       line if it is not fully terminated.

       Save the file and you're done!

       The next time you access Delphi Help, you'll receive the message,

                   "Preparing Help files for first use"

       This indicates that WinHelp is rebuilding the Delphi Help indexes to
       include HyperString. Once this is complete, you should see a new
       entry for HyperString at the bottom of the Help Contents page and
       entries for the HyperString routines should be in the Index.


    Using HyperString ----------------------------------------------------

    As noted above, HyperString is a NON-VISUAL code library.  Once
    installed, simply add "HyperStr" (and "HyperFrm" if necessary) to the
    "uses" clause of any unit where you intend to make use of HyperString
    routines.


    UnInstall ------------------------------------------------------------

    Should you decide that HyperString is not for you (unlikely but
    possible <g>), delete the 3 files mentioned above.  If you modified
    DELPHI?.CNT, simply re-edit the file and delete the 2 lines that were
    added or better yet, simply replace the file with the backup copy.


    License Agreement ----------------------------------------------------

    MIT License

    Copyright (c) 1996-2003 EFD Systems

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


    Revision History -----------------------------------------------------

    v1.0 - 96/12/25, Original release (Happy holidays).
    v1.5 - 97/06/01, Many new routines, instructions for Delphi 3.
    v2.0 - 97/08/01, Added dynamic arrays and other new features.
    v2.2 - 97/12/15, Revised dynamic arrays, more new features.
    v2.5 - 98/01/30, Bigger and better than ever!
    v2.8 - 98/05/01, Improved performance, additions, bug fixes.
    v4.0 - 98/07/30, Modified for D4, changed version to match.
    v4.2 - 98/11/22, Modified to support SubScript.
    v5.0 - 99/05/01, Added user-defined case tables, reg expressions.
                     Moved form and shell related code to HyperFrm unit.
    v6.0 - 01/09/01, Revised to support D6
